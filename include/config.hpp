#pragma once 

#include <string>
#include <map>

namespace metriffic 
{

class Config 
{
public:
    void add(const std::string& key, const std::string& value) 
    {
        cfg_[key] = value;
    } 
    void clear()
    {
        cfg_.clear();
    }
    std::string operator[](const std::string& key) const
    {
        auto fit = cfg_.find(key);
        return fit == cfg_.end() ? "" : fit->second;
    }
private:
    std::map<std::string, std::string> cfg_;
};

} // namespace metriffic
