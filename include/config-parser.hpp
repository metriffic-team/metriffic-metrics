#pragma once

#include "config.hpp"

namespace metriffic
{

struct ConfigParser
{

    bool parse(const std::string& file_name);

    Config config;
}; // struct ConfigParser

} // namespace metriffic
