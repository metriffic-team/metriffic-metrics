#include "serializer.hpp"
#include <sys/times.h>
#include <memory>
#include <fstream>
#include <algorithm>
#include <mutex>

#include <map>

namespace metriffic
{

class Metric
{
public:
    Metric(const std::string& name)
     : name_(name) {}
    virtual bool report(Serializer&) = 0;
    virtual void finish() = 0;
    virtual void tick() = 0;
    virtual void tock() = 0;

    const std::string& name() { return name_; }

protected:
    std::string name_;
}; // class Metric

class ThreadTimeMetric : public Metric
{
public:
    ThreadTimeMetric(const std::string& name) 
     : Metric(name) {}
    bool report(Serializer&) override;
    void finish() override;
    void tick() override;
    void tock() override;

private:
    int64_t  start_us;
    int64_t  max_us = 0;
    int64_t  total_us = 0;
    uint32_t count = 0;
}; // class ThreadTimeMetric

class ProcessTimeMetric : public Metric
{
public:
    ProcessTimeMetric(const std::string&);
    bool report(Serializer&) override;
    void finish() override;
    void tick() override;
    void tock() override;

private:
    tms      start;
    clock_t  start_real;

    clock_t  max_user = 0;
    clock_t  max_system = 0;
    clock_t  max_real = 0;
    clock_t  total_user = 0;
    clock_t  total_system = 0;
    clock_t  total_real = 0;
    uint32_t count = 0;
    
}; // class ProcessTimeMetric

class ProcessMemoryMetric : public Metric
{
public:
    ProcessMemoryMetric(const std::string&);
    bool report(Serializer&) override;
    void finish() override;
    void tick() override;
    void tock() override;
private:
    pid_t pid;
    int   pagesize;
}; // class ProcessMemoryMetric

} // namespace metriffic
