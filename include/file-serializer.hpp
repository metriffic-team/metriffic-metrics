#pragma once 

#include "serializer.hpp"
#include "config.hpp"
#include <sstream>
#include <fstream>
#include <mutex>
#include <memory>
#include <map>

namespace metriffic
{

class FileSerializer : public Serializer
{
    typedef std::map<std::string, std::shared_ptr<std::ofstream>> MetricFileMap;
    typedef MetricFileMap::iterator                               MetricFileMapIt;
    typedef std::pair<std::stringstream, MetricFileMapIt>         MetricWriteBuffer;

public:
    bool initialize(const Config& cfg) override;
    bool is_initialized() const override;
    bool register_metric(const std::string& metric_name) override;

public:
    Serializer& begin(const std::string metric_name) override;
    Serializer& end() override;
    Serializer& write(const std::string&, uint8_t) override;
    Serializer& write(const std::string&, uint16_t) override;
    Serializer& write(const std::string&, uint32_t) override;
    Serializer& write(const std::string&, uint64_t) override;
    Serializer& write(const std::string&, int8_t) override;
    Serializer& write(const std::string&, int16_t) override;
    Serializer& write(const std::string&, int32_t) override;
    Serializer& write(const std::string&, int64_t) override;
    Serializer& write(const std::string&, bool) override;
    Serializer& write(const std::string&, float) override;
    Serializer& write(const std::string&, double) override;

private:
    template <typename T> void write_T(const T& v)
    {
        if(write_buffer_.second != metric_files_.end()) {
            auto& buffer = write_buffer_.first;
            buffer << v << ", ";
        }
    }

private:
    bool initialized_;
    std::mutex mutex_;
    std::string logdir_;
    MetricFileMap metric_files_;
    MetricWriteBuffer write_buffer_;
}; // class FileSerializer

} // namespace metriffic
