#pragma once 

#include "serializer.hpp"
#include "config.hpp"
#include <mutex>
#include <memory>
#include <chrono>

namespace metriffic
{

class InfluxDBSerializer : public Serializer
{
public:
    InfluxDBSerializer();
    ~InfluxDBSerializer();
    bool initialize(const Config& cfg) override;
    bool is_initialized() const override;
    bool register_metric(const std::string& metric_name) override;

public:
    Serializer& begin(const std::string metric_name) override;
    Serializer& end() override;
    Serializer& write(const std::string& key, uint8_t) override;
    Serializer& write(const std::string& key, uint16_t) override;
    Serializer& write(const std::string& key, uint32_t) override;
    Serializer& write(const std::string& key, uint64_t) override;
    Serializer& write(const std::string& key, int8_t) override;
    Serializer& write(const std::string& key, int16_t) override;
    Serializer& write(const std::string& key, int32_t) override;
    Serializer& write(const std::string& key, int64_t) override;
    Serializer& write(const std::string& key, bool) override;
    Serializer& write(const std::string& key, float) override;
    Serializer& write(const std::string& key, double) override;

private:
    struct Impl;     
    std::unique_ptr<Impl> pimpl;
    bool initialized_;
    std::mutex mutex_;
    std::string url_;
    std::string logref_;
    std::chrono::steady_clock::time_point time_begin_; 
}; // class InfluxDBSerializer

} // namespace metriffic
