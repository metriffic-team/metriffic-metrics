#pragma once 

#include "config.hpp"
#include <stdint.h>
#include <string>

namespace metriffic 
{

class Serializer
{
public:
    virtual bool initialize(const Config& cfg) = 0;
    virtual bool is_initialized() const = 0;
    virtual bool register_metric(const std::string& metric_name) = 0;

    virtual Serializer& begin(const std::string metric_name) = 0;
    virtual Serializer& end() = 0;

public:
    virtual Serializer& write(const std::string&, uint8_t) = 0;
    virtual Serializer& write(const std::string&, uint16_t) = 0;
    virtual Serializer& write(const std::string&, uint32_t) = 0;
    virtual Serializer& write(const std::string&, uint64_t) = 0;
    virtual Serializer& write(const std::string&, int8_t) = 0;
    virtual Serializer& write(const std::string&, int16_t) = 0;
    virtual Serializer& write(const std::string&, int32_t) = 0;
    virtual Serializer& write(const std::string&, int64_t) = 0;
    virtual Serializer& write(const std::string&, bool) = 0;
    virtual Serializer& write(const std::string&, float) = 0;
    virtual Serializer& write(const std::string&, double) = 0;

}; // Serializer

} // namespace metriffic
