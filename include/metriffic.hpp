#pragma once 

#include "config.hpp"
#include "config-parser.hpp"
#include "common-metrics.hpp"
#include <sys/times.h>
#include <memory>
#include <iostream>
#include <algorithm>
#include <mutex>

#include <map>

namespace metriffic
{

template<class Serializer>
class Metriffic
{

public:
    Metriffic() {}
    Metriffic(Metriffic const&) = delete;
    void operator=(Metriffic const&) = delete;

public:
    bool initialize(const Config& cfg)
    {
        serializer_.initialize(cfg);
        register_metric(std::make_shared<ProcessTimeMetric>("process_timer"));
        register_metric(std::make_shared<ProcessMemoryMetric>("process_memory"));
        return is_initialized();
    }

    bool initialize_from_configfile(const std::string& cfg_file)
    {
        metriffic::ConfigParser cp;
        if(!cp.parse(cfg_file)) {
            std::cout << "Failed to parse the config file" << std::endl;
            return false;
        }
        initialize(cp.config); 
        return is_initialized();
    }

    bool is_initialized() const
    {
        return serializer_.is_initialized();
    }

    void register_metric(const std::shared_ptr<Metric> metric)
    {
        if(serializer_.is_initialized()) {
            metrics_[metric->name()] = metric;
            serializer_.register_metric(metric->name());
        }
    }

    std::shared_ptr<Metric> metric(const std::string& metric_name)
    {
        return metrics_[metric_name];
    }
    
    void report()
    {
        std::for_each(metrics_.begin(), metrics_.end(), 
                       [this](auto& mpair) {
                           mpair.second->report(this->serializer_);
                       });
    }

    void finish() const
    {
    }

private:
    Serializer serializer_;
    std::map<std::string, std::shared_ptr<Metric>> metrics_;
};

} // namespace metriffic

#ifdef ENABLE_METRIFFIC
	#define METRIFFIC(x) x
#else 
	#define METRIFFIC(x)
#endif
