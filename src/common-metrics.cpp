#include "common-metrics.hpp"

#include <iostream>
#include <sstream>
#include <time.h>
#include <unistd.h>
#include <assert.h>
#include <malloc.h>
#include <sys/resource.h>
#include <sys/types.h>

// this should go to utils 
// nanosecs in a tick, -1 if unknown
int64_t tick_factor()
{
    static int64_t tick_factor = 0;
    if (!tick_factor) {
        if ((tick_factor = ::sysconf(_SC_CLK_TCK)) <= 0) {
            tick_factor = -1;
        }  else {
            assert(tick_factor <= 1000000000LL); // logic doesn't handle large ticks
            tick_factor = 1000000000LL / tick_factor;  // compute factor
            if (!tick_factor) {
                tick_factor = -1;
            }
        }
    }
    return tick_factor;
}

int64_t cpu_time_now()
{
    struct timespec ts{};
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);

    int64_t cpu_us = 0UL;
    cpu_us += ts.tv_sec * 1000000UL;
    cpu_us += ts.tv_nsec / 1000;
    return cpu_us;
}

namespace metriffic
{

bool
ThreadTimeMetric::report(Serializer& ms)
{
    ms.begin(name_)
      .write("count",   count)
      .write("totalus", total_us)
      .write("avgus",   count ? total_us/count : 0)
      .write("maxus",   max_us)
      .end();

    return ms.is_initialized();
}

void 
ThreadTimeMetric::tick()
{
    start_us = cpu_time_now();
}

void 
ThreadTimeMetric::tock()
{
    int64_t cpu_us = cpu_time_now();

    auto diff_us = cpu_us - start_us;

    if(cpu_us > max_us) max_us = cpu_us;
    total_us += diff_us;

    count++;
}

void
ThreadTimeMetric::finish()
{
}


ProcessMemoryMetric::ProcessMemoryMetric(const std::string& name)
 : Metric(name)
{
    pid = getpid();
    pagesize = getpagesize();
}

void
ProcessMemoryMetric::tick()
{
}

void
ProcessMemoryMetric::tock()
{
}

bool
ProcessMemoryMetric::report(Serializer& ms)
{
    auto mem_info = mallinfo();
    uint32_t mem_arena_KB = mem_info.arena >> 10;
    uint32_t mem_in_use_KB = mem_info.uordblks >> 10;
    uint32_t mem_mmap_KB = mem_info.hblkhd >> 10;
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    uint32_t mem_max_rss_KB = usage.ru_maxrss;
    uint32_t mem_virtual_KB = -1;

    // Get vsz from /proc/[pid]/statm
    std::string virtual_mem_pages_string;
    std::string statm_path = "/proc/" + std::to_string(pid) + "/statm";
    std::ifstream in;
    in.open(statm_path);

    if(in.is_open()){
        in >> virtual_mem_pages_string;
        mem_virtual_KB = ((uint32_t)std::stoi(virtual_mem_pages_string) * pagesize) >> 10;
        in.close();
    }

    ms.begin(name_)
      .write("arena",   mem_arena_KB)
      .write("inuse",   mem_in_use_KB)
      .write("mmap",    mem_mmap_KB)
      .write("max_rss", mem_max_rss_KB)
      .write("virtual", mem_virtual_KB)
      .end(); 

    return ms.is_initialized();
}

void
ProcessMemoryMetric::finish()
{
}






ProcessTimeMetric::ProcessTimeMetric(const std::string& name)
 : Metric(name)
{
    start_real = times(&start);
}

void 
ProcessTimeMetric::tick()
{
}

void 
ProcessTimeMetric::tock()
{
}

bool
ProcessTimeMetric::report(Serializer& ms)
{
    tms now;
    clock_t now_real = times(&now);
    clock_t diff_real = now_real - start_real;
    clock_t diff_user = now.tms_utime - start.tms_utime;
    clock_t diff_system = now.tms_stime - start.tms_stime;

    start_real = times(&start);

    max_user = std::max(max_user, diff_user);
    max_system = std::max(max_system, diff_system);
    max_real = std::max(max_real, diff_real);

    total_user += diff_user;
    total_system += diff_system;
    total_real += diff_real;

    count++;
    ms.begin(name_)
      .write("count",      count)
      .write("user_total", (uint32_t)total_user)
      .write("user_avg",   (uint32_t)(count ? total_user/count : 0))
      .write("user_max",   (uint32_t)max_user)
      .write("sys_total",  (uint32_t)total_system)
      .write("sys_avg",    (uint32_t)(count ? total_system/count : 0))
      .write("sys_max",    (uint32_t)max_system)
      .write("real_total", (uint32_t)total_real)
      .write("real_avg",   (uint32_t)(count ? total_real/count : 0))
      .write("real_max",   (uint32_t)max_real)
      .end();

    return ms.is_initialized();
}

void
ProcessTimeMetric::finish()
{
}

} // namespace metriffic
