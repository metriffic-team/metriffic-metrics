#include "file-serializer.hpp"
#include <uuid.h>
#include <filesystem>
#include <iostream>
#include <random>


namespace metriffic
{

bool 
FileSerializer::initialize(const Config& cfg)
{
    std::string logname;
    const std::string& path = cfg["path"];
    const std::string& reference = cfg["run-reference"];
    if(reference.empty()) {
        std::lock_guard<std::mutex> lk(mutex_);
        std::random_device rd;
        auto seed_data = std::array<int, std::mt19937::state_size> {};
        std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
        std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
        std::mt19937 generator(seq);

        uuids::uuid_random_generator dgen(generator);
        uuids::uuid uid = dgen();
        logname = uuids::to_string(uid);
    } else {
        logname = reference;
    }

    int i = 0;
    do {
        logdir_ = path + std::filesystem::path::preferred_separator + "log." + 
                                        logname + (i ? "." + std::to_string(i) : "");
        i++;
    } while(std::filesystem::exists(logdir_));

    if(std::filesystem::create_directory(logdir_)) {
        std::cout << "[metriffic] successfully created folder " 
                  << logdir_
                  << std::endl;
        initialized_ = true;
    } else {
        std::cout << "[metriffic] failed to create folder " 
                  << logdir_ 
                  << std::endl;
        initialized_ = false;
    }
    return is_initialized();
}

bool
FileSerializer::register_metric(const std::string& metric_name)
{
    std::lock_guard<std::mutex> lk(mutex_);
    if(is_initialized()) {
        std::filesystem::path metric_dir(logdir_);
        std::filesystem::path metric_file(metric_name);
        std::filesystem::path metric_path = metric_dir / metric_file;
        metric_files_[metric_name] = std::make_shared<std::ofstream>(metric_path.string());
        std::cout << "[metriffic] successfully registered " << metric_name << std::endl;
        return true;
    } else {
        std::cout << "[metriffic] failed to register " << metric_name << std::endl;
        return false;
    }

}

bool
FileSerializer::is_initialized() const 
{
    return initialized_;
}

Serializer&
FileSerializer::begin(const std::string metric_name)
{
    auto mit = metric_files_.find(metric_name);
    write_buffer_.first.str("");
    write_buffer_.second = mit;
    return *this;
}

Serializer&
FileSerializer::end()
{
    if(write_buffer_.second != metric_files_.end()) {
        auto& os = *write_buffer_.second->second;
        auto& buffer = write_buffer_.first;
        os << buffer.str() << std::endl;
        write_buffer_.second = metric_files_.end();
    }

    return *this;
}


Serializer&
FileSerializer::write(const std::string&, uint8_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, uint16_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, uint32_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, uint64_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, int8_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, int16_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, int32_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, int64_t v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, bool v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, float v)
{
    write_T(v);
    return *this;
}

Serializer&
FileSerializer::write(const std::string&, double v)
{
    write_T(v);
    return *this;
}

} // namespace metriffic
