#include "influxdb-serializer.hpp"
#include <influxdb.hpp>
#include <uuid.h>
#include <iostream>
#include <ctime> 
#include <sstream>
#include <iomanip> 
#include <string> 


namespace metriffic
{

struct InfluxDBSerializer::Impl : public influxdb_cpp::builder
{
    influxdb_cpp::server_info server;
    int delim_cnt = 0;

    Impl()
       : server("", 0, "", "", "")
    {
    } 
    Impl(const std::string& ip, 
         const std::string& port,
         const std::string& dbname,
         const std::string& user,
         const std::string& pass)
      : server(ip, std::stoi(port), dbname, user, pass, "s")
    {
        //std::string resp;
        //query(resp, "show databases", server);
        //std::cout <<"SHOW: " << resp << std::endl;
        //create_db(resp, dbname, server);
        //std::cout <<"CREATE: " << resp << std::endl;
    }
    influxdb_cpp::detail::tag_caller& measurement(const std::string& m) 
    {
        lines_.str("");
        auto& ret = meas(m);
        return ret;
    }
    influxdb_cpp::detail::tag_caller& tag(const std::string& k, const std::string& v) 
    {
        return _t(k, v);
    }
    influxdb_cpp::detail::field_caller& field(const std::string& k, long long v) {
        return _f_i(delim(), k, v);
    }
    influxdb_cpp::detail::field_caller& field(const std::string& k, const std::string& v) 
    {
        return _f_s(delim(), k, v);
    }
    influxdb_cpp::detail::field_caller& field(const std::string& k, double v, int prec) 
    {
        return _f_f(delim(), k, v, prec);
    }

    influxdb_cpp::detail::ts_caller& timestamp(long long ts) 
    {
        return _ts(ts);
    }
    int post_http(std::string* resp) 
    {
        std::cout<<"TADAM: "<<lines_.str()<<std::endl;
        std::string response;
        int i = _post_http(server, &response);
        std::cout<<"RESP: "<< i <<", "<<response<<std::endl;
        return i;
    }
    char delim() 
    {
        return delim_cnt++ ? ',' : ' ';
    }
    void reset()
    {
        delim_cnt = 0;
    }
};

InfluxDBSerializer::InfluxDBSerializer()
{}

InfluxDBSerializer::~InfluxDBSerializer()
{}

bool 
InfluxDBSerializer::initialize(const Config& cfg)
{






    logref_ = cfg["reference"];
    time_begin_ = std::chrono::steady_clock::now(); 
    if(logref_.empty()) {
        std::stringstream ss;
        auto datetime = std::time(nullptr);
        ss << std::put_time(std::localtime(&datetime), "%Y-%m-%d.%X");
        logref_ = ss.str();
    }

    // "127.0.0.1", 8086, "db", "usr", "pwd");
    pimpl = std::make_unique<Impl>(cfg["ip"],
                                   cfg["port"],
                                   cfg["dbname"],
                                   cfg["usr"],
                                   cfg["pass"]);

    std::cout << "[metriffic] successfully connected to influxdb " 
              << std::endl;

    initialized_ = true;
    return is_initialized();
}

bool
InfluxDBSerializer::register_metric(const std::string&)
{
    // nothing to do for InfluxDB
    return true;
}

bool
InfluxDBSerializer::is_initialized() const 
{
    return initialized_;
}

Serializer&
InfluxDBSerializer::begin(const std::string metric_name)
{
    pimpl->measurement(metric_name);
    pimpl->tag("ref", logref_);
    return *this;
}

Serializer&
InfluxDBSerializer::end()
{
    std::chrono::steady_clock::time_point time_now = std::chrono::steady_clock::now(); 
    auto t = std::chrono::duration_cast<std::chrono::seconds>(time_now - time_begin_).count();
    pimpl->timestamp(t);
    pimpl->post_http(nullptr);
    pimpl->reset();
    return *this;
}


Serializer&
InfluxDBSerializer::write(const std::string& key, uint8_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, uint16_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, uint32_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, uint64_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, int8_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, int16_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, int32_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, int64_t v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, bool v)
{
    pimpl->field(key, v);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, float v)
{
    static int constexpr FLOAT_PRECISION = 3;
    pimpl->field(key, v, FLOAT_PRECISION);
    return *this;
}

Serializer&
InfluxDBSerializer::write(const std::string& key, double v)
{
    static int constexpr DOUBLE_PRECISION = 3;
    pimpl->field(key, v, DOUBLE_PRECISION);
    return *this;
}

} // namespace metriffic
