#include "config-parser.hpp"
#include <fstream>
#include <algorithm>
#include <iostream>

namespace metriffic
{

bool 
ConfigParser::parse(const std::string& file_name)
{
    config.clear();

    std::ifstream cfile (file_name);
    if (cfile.is_open()) {
        std::string line;
        while(getline(cfile, line)){
            line.erase(std::remove_if(line.begin(), line.end(), isspace),
                                 line.end());
            if(line[0] == '#' || line.empty()) {
                continue;
            }
            auto delimiterPos = line.find("=");
            auto name = line.substr(0, delimiterPos);
            auto value = line.substr(delimiterPos + 1);
            std::cout << name << " " << value << '\n';
            config.add(name, value);
        }
        std::cerr << "Successfully parsed config file " << file_name << "." << std::endl;
        return true;
    } else {
        std::cerr << "Couldn't open config file for reading." << std::endl;
        return false;
    }
}

} // namespace metriffic
