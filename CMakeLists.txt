cmake_minimum_required(VERSION 3.13.1)

project(metriffic)

set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_C_COMPILER   gcc-8)
set(CMAKE_CXX_COMPILER g++-8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)

set(SOURCES 
     "src/common-metrics.cpp"
     "src/config-parser.cpp"
     "src/influxdb-serializer.cpp"
     "src/file-serializer.cpp")

set(HEADERS
     "include/metriffic.hpp" 
     "include/common-metrics.hpp"
     "include/config.hpp"
     "include/config-parser.hpp"
     "include/serializer.hpp"
     "include/influxdb-serializer.hpp"
     "include/file-serializer.hpp")

# generate the static library from the sources
add_library(metriffic STATIC ${SOURCES})
include_directories(metriffic stduuid/include)
include_directories(metriffic influxdb-cpp)
include_directories(metriffic include)
set_target_properties(${PROJECT_NAME} PROPERTIES PUBLIC_HEADER "${HEADERS}")

# set the location for library installation -- i.e., /usr/lib in this case
# not really necessary in this example. Use "sudo make install" to apply
install(TARGETS ${PROJECT_NAME} DESTINATION /usr/lib
        PUBLIC_HEADER DESTINATION /usr/include/metriffic)

# dependencies on the following libraries:
#   uuid-dev
